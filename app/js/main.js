//helper function
function $$(selector, context) {
    context = context || document;
    var elements = context.querySelectorAll(selector);
    return Array.prototype.slice.call(elements);
} 

window.addEventListener("scroll", function() {
   	var scrolledHeight= window.pageYOffset;
  	$$(".parallax").forEach(function(el,index,array) { 
    var limit= el.offsetTop+ el.offsetHeight;
  	if(scrolledHeight > el.offsetTop && scrolledHeight <= limit) {
    el.style.backgroundPositionY=  (scrolledHeight - el.offsetTop) /1.5+ "px";
 
    } else {
     el.style.backgroundPositionY=  "0";
    }
     });
});

$(document).ready(function(){
	$('a[href^="#"]').on('click',function (e) {
	    e.preventDefault();

	    var target = this.hash;
	    var $target = $(target);

	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top
	    }, 600, 'swing', function () {
	        window.location.hash = target;
	    });
	});

  $('#filter-items li span').click(function() {
  
  // fetch the class of the clicked item
  var ourClass = $(this).attr('class');
  $('#work-grid').children('div').removeClass('grid-scale-show', 'grid-scale-hide');
   $('#work-grid').children('div').hide();
  // console.log(ourClass);
  // // reset the active class on all the buttons
  // $('#filterOptions li').removeClass('active');
  // // update the active state on our clicked button
  // $(this).parent().addClass('active');

  if(ourClass == 'all') {
    // show all our items
    $('#work-grid').children('div').addClass('grid-scale-show');
    $('#work-grid').children('div').show();
    
  }
  else {
    // hide all elements that don't share ourClass
    $('#work-grid').children('div:not(*[data-type="' + ourClass + '"])').addClass('grid-scale-hide');
    // $('#work-grid').children('div:not(*[data-type="' + ourClass + '"])').hide();
    $('#work-grid').children('div:not(.' + ourClass + ')').hide();

    // show all elements that do share ourClass
    $('#work-grid').children('div*[data-type="' + ourClass + '"]').addClass('grid-scale-show');
    $('#work-grid').children('div*[data-type="' + ourClass + '"]').show();
  }
  return false;
});
});

